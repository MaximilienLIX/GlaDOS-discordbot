# GlaDOS discord bot

> A little bot to make science better

## Features
### user messages
|command|description|
|-------|-----------|
|!chell|easter egg|
|!google query|generate google search link|
|!youtube query|share videos from youtube| 
|!play|play music in voice channel [experimental]|

### The first connection
Send a welcome message to an user for his first connection to a channel.

# WORK IN PROGRESS!
You can contribute to GlaDOS and implement any sort of features.