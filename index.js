const Discord = require('discord.js')
const bot = new Discord.Client()

const botConfig = require('./commands/.config/discord.js')

const Google = require('./commands/google')
const Chell = require('./commands/chell')
const Play = require('./commands/play')
const Youtube = require('./commands/youtube')
const Spotify = require('./commands/spotify')

bot.login(botConfig.token)

bot.on('ready', function() {
    bot.user.setGame("Half-Life 3").catch(console.error)
   // bot.user.setAvatar('./avatar.png').catch(console.error)
})

bot.on('guildMemberAdd', function (member) {
    member.createDM().then(function (channel) {
        return channel.send('Bienvenue sur le channel ' + member.displayName)
    }).catch(console.error)
})

bot.on('message', function (message) {
    let commandUsed = Google.parse(message) || Chell.parse(message) || Play.parse(message) || Youtube.parse(message) || Spotify.parse(message)
})

