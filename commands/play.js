const Command = require('./command')

module.exports = class Play extends Command {
    static match (message) {
        return message.content.startsWith('!play')
    }

    static action (message) {
        let voiceChannel = message.guild.channels
            .filter(function (channel) { return channel.type === 'voice' })
            .first()
        voiceChannel
            .join()
            .then(connection => { 
                const dispatcher = connection.playFile('./songs/stillAlive.mp3', { seek: 0, volume: 0.5 });
            })
            .catch(console.error);
    }
}