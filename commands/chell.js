const Command = require('./command')

module.exports = class Chell extends Command {
    static match (message) {
        return message.content.startsWith('!chell')
    }

    static action (message) {
        message.reply("The cake is a lie...")
    }
}