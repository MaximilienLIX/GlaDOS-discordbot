const Command = require('./command')
const spotifyWebApi = require('spotify-web-api-node')
const spotifyConfig = require('./.config/spotify')

var scope = ['streaming']

var spotifyApi = new spotifyWebApi({
    clientId: spotifyConfig.clientId,
    clientSecret: spotifyConfig.clientSecret
})

//var authorizeURL = spotifyApi.createAuthorizeURL(scope)

spotifyApi.clientCredentialsGrant()
    .then(function(data){
        console.log('The access token for spotify expires in ' + data.body['expires_in'])
       //console.log('The access token is ' + data.body['access_token'])

        spotifyApi.setAccessToken(data.body['access_token'])
    }, function(error){
        console.log('Something went wrong when retrieving an access token', error)
    })

module.exports = class Spotify extends Command {
    static match(message) {
        return message.content.startsWith('!spotify')
    }

    static action(message) {
        let query = message.content.split(' ').slice(1)
        let str = query.reduce((acc, ite) => acc + ' ' + ite)

        spotifyApi.searchTracks(str)
            .then(function (data) {
                console.log('Search by ' + str, data.body)
            }, function (error) {
                console.error(error)
            })
    }
}