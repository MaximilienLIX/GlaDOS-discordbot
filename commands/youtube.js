const Command = require('./command')

const YoutubeNode = require('youtube-node')
const youtubeNode = new YoutubeNode()
const youtubeConfig = require('./.config/youtube.js')

youtubeNode.setKey(youtubeConfig.key)

module.exports = class Youtube extends Command {

    static match(message) {
        return message.content.startsWith('!youtube')
    }

    static action(message) {
        return new Promise((resolve, reject) => {
            let query = message.content.split(' ').slice(1)
            let str = query.reduce((acc, ite) => acc + ' ' + ite)

            console.log(str)

            youtubeNode.search(str, youtubeConfig.max, function (error, result) {
                if (error) {
                    console.log(error)
                }
                else {
                    let links = []
                    result.items.map(item => {
                        if (typeof item['id']['videoId'] != 'undefined') {
                            links.push('https://youtu.be/' + item['id']['videoId'])
                        }
                    })
                    if (typeof links[0] != 'undefined') {
                        message.reply(links[0])
                    }
                }
            })
        })
    }
}